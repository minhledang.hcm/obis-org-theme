/** @format */

import { type FormEventHandler, useRef } from 'react';
import { clsx } from 'keycloakify/tools/clsx';
import { useConstCallback } from 'keycloakify/tools/useConstCallback';
import type { PageProps } from 'keycloakify/login/pages/PageProps';
import { useGetClassName } from 'keycloakify/login/lib/useGetClassName';
import type { KcContext } from '../kcContext';
import type { I18n } from '../i18n';
import TermAndConditionPopup, {
	PopupHandle,
} from '../../components/popup/term-and-condition.popup';
import '@patternfly/react-core/dist/styles/base.css';
import { setCookie } from '../../utils';
import { RLM_COOKIE_KEY } from '../../constants';
import { ADMIN_URL, SSO_REALM } from '../../../environment';

const loginOHBI = () => {
	setCookie(RLM_COOKIE_KEY, SSO_REALM);
	window.location.replace(ADMIN_URL);
};

export default function Login(
	props: PageProps<Extract<KcContext, { pageId: 'login.ftl' }>, I18n>
) {
	const { kcContext, i18n, doUseDefaultCss, Template, classes } = props;
	const popupRef = useRef<PopupHandle>(null);

	const { getClassName } = useGetClassName({
		doUseDefaultCss,
		classes,
	});

	// const loginOHBI = () => {
	// 	setCookie(RLM_COOKIE_KEY, SSO_REALM);
	// 	window.location.replace(ADMIN_URL);
	// };

	const {
		social,
		realm,
		url,
		// usernameHidden,
		// login,
		// auth,
		// registrationDisabled,
	} = kcContext;

	// const { msg, msgStr } = i18n;

	// const [isLoginButtonDisabled, setIsLoginButtonDisabled] = useState(false);

	const onSubmit = useConstCallback<FormEventHandler<HTMLFormElement>>((e) => {
		e.preventDefault();

		// setIsLoginButtonDisabled(true);

		const formElement = e.target as HTMLFormElement;

		//NOTE: Even if we login with email Keycloak expect username and password in
		//the POST request.
		formElement
			.querySelector("input[name='email']")
			?.setAttribute('name', 'username');
		if (kcContext.realm.name !== 'master') {
			setCookie(RLM_COOKIE_KEY, kcContext.realm.name);
		}
		formElement.submit();
	});

	return (
		<>
			<TermAndConditionPopup ref={popupRef} />
			<Template
				{...{ kcContext, i18n, doUseDefaultCss, classes }}
				headerNode={<></>}
				// displayInfo={social.displayInfo}
				displayWide={true}>
				<div
					id="kc-form"
					className={clsx(
						realm.password &&
							social.providers &&
							getClassName('kcContentWrapperClass')
					)}
					style={{ width: '100%' }}>
					<div
						id="kc-form-wrapper"
						className={clsx(
							realm.password &&
								social.providers && [
									getClassName('kcFormSocialAccountContentClass'),
									getClassName('kcFormSocialAccountClass'),
								]
						)}>
						{realm.password && (
							<form
								id="kc-form-login"
								onSubmit={onSubmit}
								action={url.loginAction}
								method="post">
								{/* <div className={getClassName('kcFormGroupClass')}>
									{!usernameHidden &&
										(() => {
											const label = !realm.loginWithEmailAllowed
												? 'username'
												: realm.registrationEmailAsUsername
													? 'email'
													: 'usernameOrEmail';

											const autoCompleteHelper: typeof label =
												label === 'usernameOrEmail' ? 'username' : label;

											return (
												<>
													<label
														htmlFor={autoCompleteHelper}
														className={getClassName('kcLabelClass')}>
														{msg(label)}
													</label>
													<input
														tabIndex={1}
														id={autoCompleteHelper}
														className={getClassName('kcInputClass')}
														//NOTE: This is used by Google Chrome auto fill so we use it to tell
														//the browser how to pre fill the form but before submit we put it back
														//to username because it is what keycloak expects.
														name={autoCompleteHelper}
														defaultValue={login.username ?? ''}
														type="text"
														autoFocus={true}
														autoComplete="off"
													/>
												</>
											);
										})()}
								</div> */}

								{/* <div className={getClassName('kcFormGroupClass')}>
									<label
										htmlFor="password"
										className={getClassName('kcLabelClass')}>
										{msg('password')}
									</label>
									<input
										tabIndex={2}
										id="password"
										className={getClassName('kcInputClass')}
										name="password"
										type="password"
										autoComplete="off"
									/>
								</div> */}

								{/* <div
									className={clsx(
										getClassName('kcFormGroupClass'),
										getClassName('kcFormSettingClass')
									)}>
									<div id="kc-form-options">
										{realm.rememberMe && !usernameHidden && (
											<div className="checkbox">
												<label>
													<input
														tabIndex={3}
														id="rememberMe"
														name="rememberMe"
														type="checkbox"
														{...(login.rememberMe === 'on'
															? {
																	checked: true,
																}
															: {})}
													/>
													{msg('rememberMe')}
												</label>
											</div>
										)}
									</div>
									<div className={getClassName('kcFormOptionsWrapperClass')}>
										{realm.resetPasswordAllowed && (
											<span>
												<a tabIndex={5} href={url.loginResetCredentialsUrl}>
													{msg('doForgotPassword')}
												</a>
											</span>
										)}
									</div>
								</div> */}

								{/* <div
									id="kc-form-buttons"
									className={getClassName('kcFormGroupClass')}>
									<input
										type="hidden"
										id="id-hidden-input"
										name="credentialId"
										{...(auth?.selectedCredential !== undefined
											? {
													value: auth.selectedCredential,
												}
											: {})}
									/>
									<input
										tabIndex={4}
										className={clsx(
											getClassName('kcButtonClass'),
											getClassName('kcButtonPrimaryClass'),
											getClassName('kcButtonBlockClass'),
											getClassName('kcButtonLargeClass')
										)}
										name="login"
										id="kc-login"
										type="submit"
										value={msgStr('doLogIn')}
										disabled={isLoginButtonDisabled}
									/>
								</div> */}
								<p className="text-powered">Powered by On Hand BI</p>
								{realm.password && (
									<div
										id="kc-social-providers"
										className={clsx(
											getClassName('kcFormSocialAccountContentClass'),
											getClassName('kcFormSocialAccountClass')
										)}>
										{/* {social.providers.length > 0 && (<div className="social-divider">or sign in with</div>)} */}
										<div className="social-divider">sign in with</div>
										<ul
											className={clsx(
												getClassName('kcFormSocialAccountListClass')
												// social.providers.splice(0, 2).length > 4 && getClassName("kcFormSocialAccountDoubleListClass")e
											)}>
											<>
												{social.providers?.slice(0, 1).map((p) => (
													<li
														key={p.providerId}
														className={getClassName(
															'kcFormSocialAccountListLinkClass'
														)}>
														<a
															href={p.loginUrl}
															id={`zocial-${p.alias}`}
															className={clsx('zocial', p.providerId)}>
															<span>{p.displayName}</span>
														</a>
													</li>
												))}
												
												{kcContext.realm.name !== SSO_REALM && kcContext.realm.name !== "master" && (
													<li
														onClick={loginOHBI}
														className={getClassName(
															'kcFormSocialAccountListLinkClass'
														)}>
														<a
															id="zocial-ohbi"
															className={clsx('zocial', 'ohbi')}>
															<span>On Hand BI</span>
														</a>
													</li>
												)}
											</>
										</ul>
									</div>
								)}

								{/* <div id="kc-info" className={getClassName('kcSignUpClass')}>
									<div
										id="kc-info-wrapper"
										className={getClassName('kcInfoAreaWrapperClass')}>
										{realm.password &&
											realm.registrationAllowed &&
											!registrationDisabled && (
												<div id="kc-registration">
													<span>
														{msg('noAccount')}
														<a tabIndex={6} href={url.registrationUrl}>
															{msg('doRegister')}
														</a>
													</span>
												</div>
											)}
									</div>
								</div> */}

								<p className="right-reserved">
									© OHBI. All rights reserved - ver 1.0.0.
								</p>
							</form>
						)}
					</div>
				</div>
			</Template>
		</>
	);
}
