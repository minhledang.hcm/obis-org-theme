import { URL_ORG } from "../../environment";
import { handleResponse } from "../utils";

interface IOrganization {
  name: string;
  loginLogo: string;
  sidebarLogo: string;
  loadingLogo: string;
}

export const getOrganizationMetadata = (orgID: string) =>
	handleResponse<IOrganization>(fetch(`${URL_ORG}/${orgID}/metadata`)).catch(_ => null);
