const RestePasswordLogo = () => {
	return (
		<svg
			width="277"
			height="277"
			viewBox="0 0 277 277"
			fill="none"
			xmlns="http://www.w3.org/2000/svg">
			<g filter="url(#filter0_d_5_1180)">
				<rect
					opacity="0.8"
					x="50.1833"
					y="48.7778"
					width="176.333"
					height="176.333"
					rx="35.2667"
					fill="white"
				/>
				<path
					d="M117.664 177.79H114.733C91.2926 177.79 79.5723 171.93 79.5723 142.629V113.328C79.5723 89.887 91.2926 78.1666 114.733 78.1666H161.615C185.056 78.1666 196.776 89.887 196.776 113.328V142.629C196.776 166.069 185.056 177.79 161.615 177.79H158.685C156.868 177.79 155.11 178.669 153.997 180.134L145.206 191.854C141.339 197.011 135.01 197.011 131.142 191.854L122.352 180.134C121.414 178.845 119.304 177.79 117.664 177.79Z"
					fill="url(#paint0_linear_5_1180)"
				/>
				<path
					d="M114.733 145.441C113.62 145.441 112.507 145.031 111.628 144.152L99.9072 132.432C98.2077 130.732 98.2077 127.919 99.9072 126.22L111.628 114.499C113.327 112.8 116.14 112.8 117.839 114.499C119.539 116.199 119.539 119.012 117.839 120.711L109.225 129.326L117.839 137.94C119.539 139.64 119.539 142.453 117.839 144.152C116.96 145.031 115.847 145.441 114.733 145.441Z"
					fill="url(#paint1_linear_5_1180)"
				/>
				<path
					d="M161.615 145.441C160.501 145.441 159.388 145.031 158.509 144.152C156.81 142.452 156.81 139.64 158.509 137.94L167.123 129.326L158.509 120.711C156.81 119.012 156.81 116.199 158.509 114.499C160.208 112.8 163.021 112.8 164.721 114.499L176.441 126.22C178.141 127.919 178.141 130.732 176.441 132.431L164.721 144.152C163.842 145.031 162.728 145.441 161.615 145.441Z"
					fill="url(#paint2_linear_5_1180)"
				/>
				<path
					d="M132.314 147.376C131.728 147.376 131.142 147.258 130.556 147.024C128.329 146.086 127.274 143.508 128.27 141.222L139.991 113.855C140.928 111.628 143.507 110.573 145.792 111.57C148.019 112.507 149.074 115.086 148.078 117.371L136.357 144.739C135.654 146.379 134.013 147.376 132.314 147.376Z"
					fill="url(#paint3_linear_5_1180)"
				/>
			</g>
			<defs>
				<filter
					id="filter0_d_5_1180"
					x="0.22224"
					y="0.286167"
					width="276.255"
					height="276.256"
					filterUnits="userSpaceOnUse"
					color-interpolation-filters="sRGB">
					<feFlood flood-opacity="0" result="BackgroundImageFix" />
					<feColorMatrix
						in="SourceAlpha"
						type="matrix"
						values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
						result="hardAlpha"
					/>
					<feOffset dy="1.46944" />
					<feGaussianBlur stdDeviation="24.9806" />
					<feComposite in2="hardAlpha" operator="out" />
					<feColorMatrix
						type="matrix"
						values="0 0 0 0 0.339365 0 0 0 0 0.590853 0 0 0 0 1 0 0 0 1 0"
					/>
					<feBlend
						mode="normal"
						in2="BackgroundImageFix"
						result="effect1_dropShadow_5_1180"
					/>
					<feBlend
						mode="normal"
						in="SourceGraphic"
						in2="effect1_dropShadow_5_1180"
						result="shape"
					/>
				</filter>
				<linearGradient
					id="paint0_linear_5_1180"
					x1="99.4917"
					y1="52.6447"
					x2="162.278"
					y2="219.574"
					gradientUnits="userSpaceOnUse">
					<stop stop-color="#5797FF" />
					<stop offset="1" stop-color="#005BD0" />
				</linearGradient>
				<linearGradient
					id="paint1_linear_5_1180"
					x1="102.507"
					y1="110.629"
					x2="125.229"
					y2="136.349"
					gradientUnits="userSpaceOnUse">
					<stop stop-color="#FFCF6F" />
					<stop offset="1" stop-color="#FFA344" />
				</linearGradient>
				<linearGradient
					id="paint2_linear_5_1180"
					x1="161.108"
					y1="110.628"
					x2="183.831"
					y2="136.349"
					gradientUnits="userSpaceOnUse">
					<stop stop-color="#FFCF6F" />
					<stop offset="1" stop-color="#FFA344" />
				</linearGradient>
				<linearGradient
					id="paint3_linear_5_1180"
					x1="131.783"
					y1="108.277"
					x2="157.497"
					y2="134.294"
					gradientUnits="userSpaceOnUse">
					<stop stop-color="#FFCF6F" />
					<stop offset="1" stop-color="#FFA344" />
				</linearGradient>
			</defs>
		</svg>
	);
};

export default RestePasswordLogo;