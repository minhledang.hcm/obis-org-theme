export async function handleResponse<T>(action: Promise<Response>): Promise<T> {
	const response = await action;
  if (response.ok) return response.json().then(res => res.data);
  const err = new Error("HTTP status code: " + response.status);
  throw err;
};