import { RLM_COOKIE_KEY } from "./keycloak-theme/constants";
import { getOrganizationMetadata } from "./keycloak-theme/services/organization.service";
import { getCookie } from "./keycloak-theme/utils";

const realm = getCookie(RLM_COOKIE_KEY);
export const metadata = await getOrganizationMetadata(realm);