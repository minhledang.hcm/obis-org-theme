FROM alpine/git as clone-obis-theme-1
WORKDIR /app
RUN git clone https://gitlab.com/minhledang.hcm/obis-theme.git
COPY environment.ts obis-theme/src/

FROM node:20 as build-obis-theme
WORKDIR /app
COPY --from=clone-obis-theme-1 /app/obis-theme .

RUN apt-get update && apt-get install -y maven
RUN apt-get install -y zip

RUN npm install -g pnpm
RUN pnpm i
RUN pnpm build-keycloak-theme

# Install and build obis org theme
FROM alpine/git as clone-obis-org-theme-1
WORKDIR /app
RUN git clone https://gitlab.com/minhledang.hcm/obis-org-theme.git
COPY environment.ts obis-org-theme/src/

FROM node:20 as build-obis-org-theme
WORKDIR /app
COPY --from=clone-obis-org-theme-1 /app/obis-org-theme .

RUN apt-get update && apt-get install -y maven
RUN apt-get install -y zip

RUN npm install -g pnpm
RUN pnpm i
RUN pnpm build-keycloak-theme


# RUN yarn install --frozen-lockfile
# RUN yarn build-keycloak-theme

# Build Keycloak
FROM quay.io/keycloak/keycloak:24.0.2 as builder

WORKDIR /opt/keycloak

# Copy the custom themes into the Keycloak distribution
COPY --from=build-obis-theme --chown=1000:0 /app/theme/*.jar /opt/keycloak/providers/
COPY --from=build-obis-org-theme --chown=1000:0 /app/theme/*.jar /opt/keycloak/providers/

ENV KC_FEATURES=account3,admin-fine-grained-authz,declarative-ui

# for demonstration purposes only, please make sure to use proper certificates in production instead
RUN /opt/keycloak/bin/kc.sh build

FROM quay.io/keycloak/keycloak:latest
COPY --from=builder /opt/keycloak/ /opt/keycloak/

# change these values to point to a running postgres instance
ENV KC_HOSTNAME=localhost
ENV KEYCLOAK_ADMIN=root
ENV KEYCLOAK_ADMIN_PASSWORD=root


ENTRYPOINT ["/opt/keycloak/bin/kc.sh", "start-dev"]

EXPOSE 8080